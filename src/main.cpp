#include "ros/ros.h"
#include "std_msgs/Int8.h"
#include "std_msgs/Float32.h"
#include "std_msgs/Bool.h"
#include "std_msgs/Int8MultiArray.h"
#include "std_msgs/Int32MultiArray.h"
#include "std_msgs/Float64MultiArray.h"
// #include "std_msgs/String.h"
#include "sensor_msgs/BatteryState.h"
#include "mrs_msgs/UavStatus.h"
#include "mrs_msgs/HwApiStatus.h"
#include "mrs_msgs/ControlManagerDiagnostics.h"

// Eagle dependencies
// #include "vofod/Status.h"
// #include "lidar_tracker/Tracks.h"

// #include "eagle_msgs/vofod/Status.h"
// #include "eagle_msgs/lidar_tracker/Tracks.h"

#include "eagle_msgs/DetectionStatus.h"
#include "eagle_msgs/Tracks.h"

// MRS System topics
#define BATTERY_TOPIC "hw_api/battery_state"
#define HW_STATUS_TOPIC "hw_api/status"
#define CTRL_DIAG_TOPIC "control_manager/diagnostics"
#define DETECTOR_STATUS_TOPIC "vofod/status"
#define TRACKS_TOPIC "lidar_tracker/tracks"

// RC topics
#define BATTERY_TOPIC_OUT "battery_state"
#define STATUS_TOPIC_OUT "uav_status"
#define DETECTOR_STATUS_TOPIC_OUT "detector_status"
#define TRACKS_ID_TOPIC_OUT "tracker/ids"
#define TRACKS_AGES_TOPIC_OUT "tracker/ages"
#define TRACKS_SEL_TOPIC_OUT "tracker/selected"

enum UavState{
    UAV_UNDEFINED,
    DISARMED,
    ARMED,
    TAKEOFF,
    LANDING,
    MANUAL,
    AUTONOMOUS
};

enum DetectorState{
    DETECTOR_UNDEFINED,
    DISABLED,
    ENABLED,
    ACTIVE
};

enum Tracker{
    NULL_TRACKER,
    LANDOFF_TRACKER,
    AUTO_TRACKER
};

class TopicAggregator {
public:
    TopicAggregator() {
        // Subscribe to input topics
        subBattery = nh.subscribe(BATTERY_TOPIC, 10, &TopicAggregator::callbackBattery, this);
        subHwStatus = nh.subscribe(HW_STATUS_TOPIC, 10, &TopicAggregator::callbackHwStatus, this);
        subCtrlDiag = nh.subscribe(CTRL_DIAG_TOPIC, 10, &TopicAggregator::callbackCtrlDiag, this);
        subDetectorDiag = nh.subscribe(DETECTOR_STATUS_TOPIC, 10, &TopicAggregator::callbackDetectorStatus, this);
        subTracks = nh.subscribe(TRACKS_TOPIC, 10, &TopicAggregator::callbackTracks, this);

        // Advertise output topics
        pubBattery = nh.advertise<std_msgs::Float32>(BATTERY_TOPIC_OUT, 0);
        pubUavStatus = nh.advertise<std_msgs::Int8>(STATUS_TOPIC_OUT, 0, true);
        pubDetectorState = nh.advertise<std_msgs::Int8>(DETECTOR_STATUS_TOPIC_OUT, 0);
        pubTrackIds = nh.advertise<std_msgs::Int32MultiArray>(TRACKS_ID_TOPIC_OUT, 0);
        pubTrackAges = nh.advertise<std_msgs::Float64MultiArray>(TRACKS_AGES_TOPIC_OUT, 0);
        pubTrackSelected = nh.advertise<std_msgs::Int8MultiArray>(TRACKS_SEL_TOPIC_OUT, 0);
    }

    void callbackBattery(const sensor_msgs::BatteryState::ConstPtr& msg) {
        // Process data
        std_msgs::Float32 processed_msg;
        processed_msg.data = msg->percentage;

        // Publish processed data
        pubBattery.publish(processed_msg);
    }

    void callbackCtrlDiag(const mrs_msgs::ControlManagerDiagnostics::ConstPtr& msg) {

        if (msg->active_tracker == "LandoffTracker"){
            tracker = LANDOFF_TRACKER;

            if (uavState == ARMED){
                updateState(TAKEOFF);
            } else if (uavState != TAKEOFF){
                updateState(LANDING);
            }
        } else if (msg->active_tracker == "NullTracker" && uavState == LANDING){
            tracker = NULL_TRACKER;
        } else if (msg->joystick_active){
            updateState(MANUAL);
        } else if (msg->flying_normally){
            tracker = AUTO_TRACKER;

            updateState(AUTONOMOUS);
        }
    }

    void callbackHwStatus(const mrs_msgs::HwApiStatus::ConstPtr& msg) {
        if (!msg->armed){
            updateState(DISARMED);
        } else if (uavState == DISARMED || msg->mode == "AUTO.LOITER" || tracker == NULL_TRACKER){
            updateState(ARMED);
        }
    }

    void callbackDetectorStatus(const eagle_msgs::DetectionStatus::ConstPtr msg){
        if (msg->detection_active){
            detectorState = ACTIVE;
        } else if (msg->detection_enabled){
            detectorState = ENABLED;
        } else {
            detectorState = DISABLED;
        }
    }

    void callbackTracks(const eagle_msgs::Tracks::ConstPtr msg){
        // Determine the length of the tracks array
        int numTracks = msg->tracks.size();
        std::vector<int> newTrackIds = {};
        std::vector<double> newTrackAges = {};
        std::vector<int8_t> newTrackSelected = {};
        std::vector<ros::Time> newTrackTimestamps = {};
        for (int i = 0; i < numTracks; i++){
            eagle_msgs::Track trackNew = msg->tracks[i];

            newTrackIds.push_back(trackNew.id);
            newTrackAges.push_back(trackNew.last_detection_age);
            if (trackNew.selected){
                newTrackSelected.push_back(1);
            } else {
                newTrackSelected.push_back(0);
            }

            std::cout << "new entry: " << trackNew.id << ", time: " << trackNew.last_detection_age << "\n";
        }

        trackIds = newTrackIds;
        trackAges = newTrackAges;
        trackSelected = newTrackSelected;
    }

    void updateState(UavState state){

        if (state == uavState){
            return;
        }

        uavState = state;
    }

    void publishUavStatus() {
        // Create and publish the uav_status message
        std_msgs::Int8 uav_status_msg;
        uav_status_msg.data = static_cast<int8_t>(uavState);
        pubUavStatus.publish(uav_status_msg);
    }

    void publishDetectorStatus() {
        // Create and publish the uav_status message
        std_msgs::Int8 detector_status_msg;
        detector_status_msg.data = static_cast<int8_t>(detectorState);
        pubDetectorState.publish(detector_status_msg);
    }

    void publishTracks() {
        // Create and publish the uav_status message
        std_msgs::Int32MultiArray tracker_ids_msg;
        tracker_ids_msg.data = trackIds;
        pubTrackIds.publish(tracker_ids_msg);

        std_msgs::Float64MultiArray tracker_ages_msg;
        tracker_ages_msg.data = trackAges;
        pubTrackAges.publish(tracker_ages_msg);

        std_msgs::Int8MultiArray tracker_selected_msg;
        tracker_selected_msg.data = trackSelected;
        pubTrackSelected.publish(tracker_selected_msg);
    }

private:
    ros::NodeHandle nh;

    ros::Subscriber subBattery;
    ros::Subscriber subHwStatus;
    ros::Subscriber subCtrlDiag;
    ros::Subscriber subDetectorDiag;
    ros::Subscriber subTracks;

    ros::Publisher pubBattery;
    ros::Publisher pubUavStatus;
    ros::Publisher pubDetectorState;
    ros::Publisher pubTrackIds;
    ros::Publisher pubTrackAges;
    ros::Publisher pubTrackSelected;

    UavState uavState = UAV_UNDEFINED;
    DetectorState detectorState = DISABLED;
    Tracker tracker = NULL_TRACKER;

    // lidar tacker
    std::vector<int> trackIds;
    std::vector<double> trackAges;
    std::vector<int8_t> trackSelected;
};

int main(int argc, char **argv) {
    // Initialize ROS node
    ros::init(argc, argv, "topic_aggregator");

    // Create an instance of TopicAggregator
    TopicAggregator node;

    // Set the loop rate to 1Hz (publish every second)
    ros::Rate loop_rate(10);

    // Publish the uav_status every second
    while (ros::ok()) {
        ros::spinOnce();

        // Publish the current uav_status
        node.publishUavStatus();
        node.publishDetectorStatus();
        node.publishTracks();

        // Sleep to maintain the loop rate
        loop_rate.sleep();
    }

    return 0;
}
